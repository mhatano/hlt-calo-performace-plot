#ifndef MYANALYSISALG_H 
#define MYANALYSISALG_H

#include "AsgTools/AnaToolHandle.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "TH1.h"
#include "TH2.h"

class ITHistSvc;
namespace xAOD{
  class IParticle;
}
 
class MyAnalysisAlg:public AthAlgorithm
{
 public:
  MyAnalysisAlg(const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode initialize();
  StatusCode finalize();
  StatusCode execute();

 private:
  ITHistSvc *m_thistSvc;
  asg::AnaToolHandle<Trig::IBunchCrossingTool> m_bct;

  int numberCaloCluster;
  int numberHLTCaloCluster;
  int numberHLTCaloCluster_topo;
  int numberHLTCaloCluster_slw;

  TH1F *reso, *reso_before;
  TH1F *actual_mu, *actual_mu_before;

  double distance(const xAOD::IParticle*,const xAOD::IParticle*);
};

#endif // MYANALYSISALG_H
