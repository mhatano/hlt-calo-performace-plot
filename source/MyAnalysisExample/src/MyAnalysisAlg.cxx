#include "MyAnalysisExample/MyAnalysisAlg.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "GaudiKernel/ITHistSvc.h"
#include "TMath.h"

MyAnalysisAlg::MyAnalysisAlg(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator)
{ }

StatusCode MyAnalysisAlg::initialize() {
  ATH_MSG_INFO("initialize()");

  numberCaloCluster = 0;
  numberHLTCaloCluster = 0;
  numberHLTCaloCluster_topo = 0;
  numberHLTCaloCluster_slw = 0;
  
  m_bct.setTypeAndName("Trig::LHCBunchCrossingTool/BunchCrossingTool");
  m_bct.initialize().setChecked();
  
  service("THistSvc", m_thistSvc).setChecked();

  reso = new TH1F("reso","Resolution",100,-0.05,0.05);
  m_thistSvc->regHist("/Myhist/reso",reso).setChecked();
  reso_before = new TH1F("reso_before","Resolution",100,-0.05,0.05);
  m_thistSvc->regHist("/Myhist/reso_before",reso_before).setChecked();

  actual_mu = new TH1F("actual_mu", "Actual mu", 1000, 0, 100);
  m_thistSvc->regHist("/Myhist/actual_mu", actual_mu).setChecked();
  actual_mu_before = new TH1F("actual_mu_before", "Actual mu in First 12BCs", 1000, 0, 100);
  m_thistSvc->regHist("/Myhist/actual_mu_before", actual_mu_before).setChecked();

  return StatusCode::SUCCESS;
}

StatusCode MyAnalysisAlg::finalize() {
  ATH_MSG_INFO( "finalize()" );
  ATH_MSG_INFO( " CaloCluster : " << numberCaloCluster );
  ATH_MSG_INFO( " HLTCaloCluster : " << numberHLTCaloCluster );
  ATH_MSG_INFO( "   Topo Clustering : " << numberHLTCaloCluster_topo );
  ATH_MSG_INFO( "   Sliding Window  : " << numberHLTCaloCluster_slw );
  return StatusCode::SUCCESS;
}

StatusCode MyAnalysisAlg::execute() {
  ATH_MSG_INFO("execute()");

  const xAOD::EventInfo* eventInfo = 0;
  evtStore()->retrieve(eventInfo).setChecked();
  ATH_MSG_INFO( "RUN" << eventInfo->runNumber() << " LB" << eventInfo->lumiBlock() << " Event" << eventInfo->eventNumber() );
  ATH_MSG_INFO( "Actual mu : " << eventInfo->actualInteractionsPerCrossing() );

  uint32_t bcid = eventInfo->bcid();
  if ( !m_bct->isFilled(bcid) ){
    ATH_MSG_INFO( "Not Filled Bunch" ); 
    return StatusCode::SUCCESS;
  }
  int bcposition = m_bct->distanceFromFront( bcid, Trig::IBunchCrossingTool::BunchCrossings );
  int gap = m_bct->gapBeforeTrain(bcid, Trig::IBunchCrossingTool::BunchCrossings);
  int train_length = m_bct->distanceFromFront(bcid, Trig::IBunchCrossingTool::BunchCrossings) + m_bct->distanceFromTail(bcid, Trig::IBunchCrossingTool::BunchCrossings) + 1;
  ATH_MSG_INFO( " BCID : " << bcid << ", Position In Train : " << bcposition << ", Gap Before This Train : " << gap << 
", Train Length : " << train_length );

  bool begining_bunch = ( bcposition < 12 );

  actual_mu->Fill( eventInfo->actualInteractionsPerCrossing() ); 
  if( begining_bunch )
   actual_mu_before->Fill( eventInfo->actualInteractionsPerCrossing() );

  const xAOD::CaloClusterContainer* offline_clusters = 0;
  evtStore()->retrieve( offline_clusters, "CaloCalTopoClusters" ).setChecked();
  if( offline_clusters != 0 ){
    numberCaloCluster += offline_clusters->size();
    ATH_MSG_INFO( " Calo Cluster Size : " << offline_clusters->size() ); 
  }

  const xAOD::CaloClusterContainer* HLT_clusters = 0;
  evtStore()->retrieve( HLT_clusters, "HLT_xAOD__CaloClusterContainer_TrigCaloClusterMaker" ).setChecked();
  if( HLT_clusters != 0){
    numberHLTCaloCluster += HLT_clusters->size();
    ATH_MSG_INFO( " HLT Cluster Size : " << HLT_clusters->size() );
    for(auto cluster:*HLT_clusters){
      if( cluster->clusterSize() == xAOD::CaloCluster::Topo_420 )
	numberHLTCaloCluster_topo += 1;
      if( cluster->clusterSize() == xAOD::CaloCluster::SW_55ele || cluster->clusterSize() == xAOD::CaloCluster::SW_37ele )
	numberHLTCaloCluster_slw += 1;
    }
  }

  if ( offline_clusters == 0 || HLT_clusters == 0 )
    return StatusCode::SUCCESS;
  
  for(auto cluster:*offline_clusters){
    if( cluster->et()<3000 ) continue;

    double dR = 1.;
    const xAOD::CaloCluster* HLT_cluster = NULL;
    for (auto hltcluster:*HLT_clusters){
      if ( hltcluster->clusterSize() != xAOD::CaloCluster::Topo_420 ) continue;
      double dRtmp = distance(cluster,hltcluster);
      if(dRtmp<dR){
	dR=dRtmp;
	HLT_cluster = hltcluster;
      }
    }

    if(dR>0.001) continue;
    double ratio = (cluster->et()-HLT_cluster->et())/cluster->et();
   
    if( fabs(cluster->eta())>0.8 ) continue;

    reso->Fill(ratio);
    if(begining_bunch)
      reso_before->Fill(ratio);
  }


  return StatusCode::SUCCESS;
}


double MyAnalysisAlg::distance(const xAOD::IParticle* A,const xAOD::IParticle* B) {
  return TMath::Sqrt((A->phi()-B->phi())*(A->phi()-B->phi())+(A->eta()-B->eta())*(A->eta()-B->eta()));
}
