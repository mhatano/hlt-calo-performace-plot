atlas_subdir( MyAnalysisExample )

find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )

atlas_add_component( MyAnalysisExample
                       src/components/*.cxx
		       src/*.cxx
		     INCLUDE_DIRS
		       ${ROOT_INCLUDE_DIRS}
		     LINK_LIBRARIES
		       AthenaBaseComps
 		       ${ROOT_INCLUDE_DIRS}
		       GaudiKernel
		       xAODCaloEvent
		       xAODJet
		       TrigBunchCrossingTool)

atlas_install_joboptions( share/*.py )
