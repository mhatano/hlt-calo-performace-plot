import AthenaPoolCnvSvc.ReadAthenaPool

svcMgr.EventSelector.InputCollections = jps.AthenaCommonFlags.FilesInput()
theApp.EvtMax = -1

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output = [ "Myhist DATAFILE='test17.root' OPT='RECREATE'" ]

from MyAnalysisExample.MyAnalysisExampleConf import *
athAlgSeq += MyAnalysisAlg('MyAnalysisAlg1', OutputLevel = INFO)

from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource.set_Value_and_Lock('data')
from TrigBunchCrossingTool.BunchCrossingTool import BunchCrossingTool
ToolSvc += BunchCrossingTool( "LHC" )


